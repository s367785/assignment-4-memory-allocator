#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>



extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t query) { return query / getpagesize() + ((query % getpagesize()) > 0); }

static size_t round_pages(size_t query) {
    return getpagesize() * pages_count(query); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

static struct block_header *heap = NULL;


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON | additional_flags, -1, 0);
}

static struct region alloc_region(void const *addr, size_t query) {
    size_t actual_size = region_actual_size(query + offsetof(struct block_header, contents));
    struct block_header *real_addr = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);
    bool extends = true;
    if (real_addr == MAP_FAILED) {
        real_addr = map_pages(addr, actual_size, 0);
        if (real_addr == MAP_FAILED) {
            return (struct region) {0};
        }
        extends = false;
    }
    block_init(real_addr, (block_size) {actual_size}, NULL);
    return (struct region) {real_addr, actual_size, extends};
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    heap = region.addr;
    return heap;
}


void heap_term(){
    struct block_header *region_start = heap;
    while (region_start){
        struct block_header *current_block = region_start;

        size_t region_size = size_from_capacity(current_block->capacity).bytes;

        while (current_block->next == block_after(current_block)){
            current_block = current_block->next;
            region_size += size_from_capacity(current_block->capacity).bytes;
        }

        struct block_header *ready_for_free = region_start;
        region_start = current_block->next;
        if(munmap(ready_for_free, round_pages(region_size)) == -1){
            exit(EXIT_FAILURE);
        }
    }
}


#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static void split_block(struct block_header *block, size_t query){
    struct block_header *new_block = (struct block_header *) (block->contents + query);
    new_block->is_free = true;
    new_block->capacity = capacity_from_size((block_size) {.bytes = block->capacity.bytes - query});
    new_block->next = block->next;
    block->next = new_block;
    block->capacity.bytes = query;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) {
        split_block(block, query);
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static void merge(struct block_header *block){
    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;
}

static bool try_merge_with_next(struct block_header *block) {
    if (block->next && mergeable(block, block->next)) {
        merge(block);
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED
    } type;
    struct block_header *block;
};

static inline bool block_is_good(struct block_header * block, size_t sz){
    return block->is_free && block_is_big_enough(sz, block);
}

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    while (block->next) {

        while (try_merge_with_next(block));

        if (block_is_good(block, sz)) {
            return (struct block_search_result)  {BSR_FOUND_GOOD_BLOCK, block};
            break;
        }
        if (block->next) {
            block = block->next;
        }
    }

    if (block_is_good(block, sz)) {
        return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};
    }

    return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result res = find_good_or_last(block, query);
    if (res.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(res.block, query);
        res.block->is_free = false;
    }
    return res;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    struct region new_reg = alloc_region(block_after(last), query);
    last->next = new_reg.addr;
    if(try_merge_with_next(last) && new_reg.extends)
        return last;
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result result = try_memalloc_existing(query, heap_start);

    if (result.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header *new_reg = grow_heap(result.block, query);
        if(!new_reg) {
            return NULL;
        }
        result = try_memalloc_existing(query, new_reg);
    }

    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        return result.block;
    }

    return NULL;
}

void *_malloc(size_t query) {
    if(query > 0) {
        struct block_header *const addr = memalloc(query, (struct block_header *) heap);
        if (addr) return addr->contents;
    }
    return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));

}
