#define _DEFAULT_SOURCE

#include "assert.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>
#include <sys/mman.h>


#define HEAP_SIZE_DEFAULT 4096
#define HEAP_SIZE_LESS_THAN_PAGE 4000
#define HEAP_SIZE_DOUBLE_PAGE 8192
#define HEAP_SIZE_ZERO 0
#define BLOCK_SIZE_1 100
#define BLOCK_SIZE_2 4000

#define get_header(mem) ((struct block_header*) (((uint8_t*)(mem)) - offsetof(struct block_header, contents)))

void* map_pages(void const* addr, size_t length, int additional_flags){
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON | additional_flags, -1, 0);
}
int test_counter = 0;

void* test_heap_init(int size){
    printf("Heap init. Size: %d\n", size);
    void* heap = heap_init(size);
    assert(heap);
    printf("Heap init success. Size: %d\n", size);
    return heap;
}

void test_simple_malloc(){
    printf("Test %d:\n", ++test_counter);

    test_heap_init(HEAP_SIZE_DEFAULT);

    printf("Memory allocation. Size: %d\n", HEAP_SIZE_LESS_THAN_PAGE);
    void* mem = _malloc(HEAP_SIZE_LESS_THAN_PAGE);
    assert(mem);
    printf("Memory allocation success. Size:%d\n", HEAP_SIZE_LESS_THAN_PAGE);

    printf("Memory allocation. Size: %d\n", HEAP_SIZE_DOUBLE_PAGE);
    mem = _malloc(HEAP_SIZE_DOUBLE_PAGE);
    assert(mem);
    printf("Memory allocation success. Size: %d\n", HEAP_SIZE_DOUBLE_PAGE);

    printf("Test %d passed!\n\n", test_counter);
    heap_term();
}

void test_block_free(){
    printf("Test %d:\n", ++test_counter);

    test_heap_init(HEAP_SIZE_ZERO);

    printf("Blocks init. Size: %d, %d\n", BLOCK_SIZE_1, BLOCK_SIZE_2);
    void* first_block = _malloc(BLOCK_SIZE_1);
    void* second_block = _malloc(BLOCK_SIZE_2);
    assert(first_block);
    assert(second_block);
    printf("Blocks init success.\n");

    _free(second_block);
    assert(!get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);
    printf("Test %d passed!\n\n", test_counter);
    heap_term();
}

void test_two_blocks_free(){
    printf("Test %d:\n", ++test_counter);

    test_heap_init(HEAP_SIZE_ZERO);

    printf("Blocks init. Size: %d, %d\n", BLOCK_SIZE_1, BLOCK_SIZE_2);
    void* first_block = _malloc(BLOCK_SIZE_1);
    void* second_block = _malloc(BLOCK_SIZE_2);
    assert(first_block);
    assert(second_block);
    printf("Blocks init success. Size: %d, %d\n", BLOCK_SIZE_1, BLOCK_SIZE_2);

    _free(first_block);
    _free(second_block);
    assert(get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);

    printf("Test %d passed!\n\n", test_counter);
    heap_term();
}

void test_region_expand(){
    printf("Test %d:\n", ++test_counter);


    struct region* heap = test_heap_init(HEAP_SIZE_ZERO);

    size_t initial_region_size = heap->size;

    printf("Large block init. Size: %d\n", 11*HEAP_SIZE_DEFAULT);
    _malloc(11*HEAP_SIZE_DEFAULT);
    size_t expanded_region_size = heap->size;
    printf("Block init success. Size: %d\n", 11*HEAP_SIZE_DEFAULT);

    assert(initial_region_size < expanded_region_size);
    printf("Test %d passed!\n\n", test_counter);
    heap_term();
}

void test_different_regions_expansion(){
    printf("Test %d:\n", ++test_counter);

    void* took_heap_start = map_pages(HEAP_START, BLOCK_SIZE_1, MAP_FIXED);
    assert(took_heap_start);
    void* allocated_filled_ptr = _malloc(BLOCK_SIZE_1);
    assert(allocated_filled_ptr);
    assert(took_heap_start < allocated_filled_ptr);

    printf("Test %d passed!\n\n", test_counter);
    heap_term();
}

int main(){
    test_simple_malloc();
    test_block_free();
    test_two_blocks_free();
    test_region_expand();
    test_different_regions_expansion();

    printf("\nALL TEST PASSED!\n");
    return 0;
}
